/**
 * Hiermit wird ein Interface f�r die Queue erstellt.
 * author: Stephanie Kaswurm, Anna-Lena Klaus
 */
public interface QueueInterface {

    /**
     * Hierbei werden drei Methoden benutzt:
     * 
     * - addElement f�gt ein neues Element dem Array hinzu
     * @param element wird im Datentyp int dargestellt und ins Array hinzugef�gt.
     * 
     * - removeElement l�scht das erste Element im Array
     * 
     * - isEmpty �berpr�ft ob das Array leer ist.
     * Der boolean �berpr�ft ob er wahr ist, dann ist das Array leer, asonsten ist es falsch, somit enth�lt das Array Elemente.
     */
    void addElement(int element);

    void removeElement();

    boolean isEmpty();

}